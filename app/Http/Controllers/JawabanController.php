<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\QuestModel;
use App\Models\AnswerModel;

class JawabanController extends Controller
{
    public function index(){
        $answers = AnswerModel::getAll();
        return view('larahub.index', compact('answers'));
    }

    public function create($id){
        return view('larahub.formanswer',compact('id'));
    }
    
    public function store(Request $request){
        // dd($request->all());
        $data = $request->all();
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        unset($data["_token"]);
        $question = AnswerModel::save($data);
        
    }

    public function show($id){
        $answer = AnswerModel::findById($id);

        return view('larahub.show', compact('quest'));
    }

    public function lihat($id){
        return view('larahub.editanswer',compact('id'));
    }

    public function edit($id){
        $answer = AnswerModel::findById($id);
        return compact('answer');
    }

    public function update($id, Request $request){
        $data = $request->all();
        $data['updated_at'] = Carbon::now();

        $answer = AnswerModel::update($id, $data);

        // return redirect('/pertanyaan/'.$quest->pertanyaan_id);
    }

    public function destroy($id){
        $quest = AnswerModel::findByAnswerId($id);
        $answer = AnswerModel::destroy($id);

        // return redirect('/pertanyaan/'.$quest->pertanyaan_id);
    }
}

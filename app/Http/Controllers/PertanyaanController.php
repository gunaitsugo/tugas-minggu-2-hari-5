<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\QuestModel;
use App\Models\AnswerModel;

class PertanyaanController extends Controller
{
    public function index(){
        $quests = QuestModel::getAll();
        return $quests;
    }

    public function create(){
        return view('larahub.formquest');
    }
    
    public function store(Request $request){
        $data = $request->all();
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        // dd($data);
        unset($data["_token"]);
        $question = QuestModel::save($data);
    }

    public function lihat($id){
        return view('larahub.show',compact('id'));
    }

    public function show($id){
        $quest = QuestModel::findById($id);
        $answers = AnswerModel::getAll($id);

        return compact(['quest','answers']);
    }

    public function edit($id){
        return view('larahub.edit', compact('id'));
    }

    public function update($id, Request $request){
        $data = $request->all();
        $data['updated_at'] = Carbon::now();
        $quest = QuestModel::update($id, $data);

        // return redirect('/pertanyaan');
    }

    public function destroy($id){
        $quest = QuestModel::destroy($id);

        // return redirect('/pertanyaan');
    }

}

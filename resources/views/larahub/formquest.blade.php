@extends('adminlte.master')

@section('content')
    <form @submit.prevent="addQuest" method="post" id="form-input">
        <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" v-model="judul" placeholder="Judul">
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" v-model="isi" placeholder="Isi">
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection

@push('scripts')
<script>
    new Vue({
        el:"#form-input",
        data: {
            judul : "",
            isi : "",
            quests : [],
            answers : []
        },
        methods: {
            addQuest : function(){
                let judul = this.judul.trim();
                let isi = this.isi.trim();
                if(judul){
                    // POST /someUrl
                    this.$http.post('/api/quest', {judul: judul, isi: isi}).then(response => {
                        
                        window.location="/pertanyaan";
                        
                    });
                }
            }
        }
    });
</script>
@endpush
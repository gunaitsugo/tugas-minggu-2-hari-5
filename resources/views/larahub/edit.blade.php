 @extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan</h3>
            </div>
            <form @submit.prevent="editQuest(quests)" id="edit-quest">
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" v-model="quests.judul" name="judul" placeholder="Judul">
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi</label>
                        <input type="text" class="form-control" id="isi" v-model="quests.isi" name="isi" placeholder="Judul">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    new Vue({
        el:"#edit-quest",
        data: {
            quests : {
                judul : "",
                isi : "",
            }
        },
        methods: {
            editQuest : function(quest){
                let judul = this.quests.judul;
                let isi = this.quests.isi;

                // console.log(isi);
                if(quest.id){
                    // POST /someUrl
                    this.$http.post('/api/quest/update/'+quest.id, {judul: judul, isi: isi}).then(response => {
                        
                        window.location="/pertanyaan";
                        
                    });
                }
            }

        },
        mounted: function() {
            // GET /someUrl
            this.$http.post('/api/quest/' + {{$id}}).then(response => {

                // get body data
                let resultQuest = response.body.quest;
                this.quests = resultQuest;
                console.log(resultQuest);

            });
        }
    });
</script>
@endpush
@extends('adminlte.master')

@section('content')
    <form @submit.prevent="addAnswer" id="form-input">
        <div class="card-body">
        <h3>  Pertanyaan : @{{ quests.isi }} </h3>
            <input type="hidden" class="form-control" id="pertanyaan_id" v-model="quests.id" name="pertanyaan_id" >
            <div class="form-group">
                <label for="isi">Jawaban</label>
                <input type="text" class="form-control" id="isi" v-model="isi" name="isi" placeholder="Jawaban">
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection

@push('scripts')
<script>
    new Vue({
        el:"#form-input",
        data: {
            pertanyaan_id : "",
            isi : "",
            quests : []
        },
        methods: {
            addAnswer : function(){
                let pertanyaan_id = this.pertanyaan_id;
                let isi = this.isi.trim();
                if(isi){
                    // console.log(pertanyaan_id);
                    // POST /someUrl
                    this.$http.post('/api/answer', {pertanyaan_id: pertanyaan_id, isi: isi}).then(response => {
                        
                        window.location="/pertanyaan/"+pertanyaan_id;
                        
                    });
                }
            }
        },
        mounted: function() {
            // GET /someUrl
            this.$http.post('/api/quest/' + {{$id}}).then(response => {

                // get body data
                let resultQuest = response.body.quest;
                // console.log(resultQuest);
                this.quests = resultQuest;
                this.pertanyaan_id = resultQuest.id;

            });
        }
    });
</script>
@endpush
@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Jawaban</h3>
            </div>
            <form @submit.prevent="editAnswer(answers)" id="edit-answer">
                <div class="card-body">
                    <div class="form-group">
                        <label for="isi">Jawaban</label>
                        <input type="text" class="form-control" id="isi" v-model="answers.isi" name="isi" placeholder="Jawaban">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    new Vue({
        el:"#edit-answer",
        data: {
            answers : {
                isi : "",
            }
        },
        methods: {
            editAnswer : function(answer){
                let isi = this.answers.isi;

                if(answer.id){
                    // POST /someUrl
                    this.$http.post('/api/answer/update/'+answer.id, {isi: isi}).then(response => {
                        
                        window.location="/pertanyaan/"+answer.pertanyaan_id;
                        
                    });
                }
            }

        },
        mounted: function() {
            // GET /someUrl
            this.$http.post('/api/answer/' + {{$id}}).then(response => {

                // get body data
                let resultAnswer = response.body.answer;
                this.answers = resultAnswer;
                console.log(resultAnswer);

            });
        }
    });
</script>
@endpush
@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Data Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body" id="pertanyaanku">
    <a href="/pertanyaan/create" class="btn btn-primary mb-2">
        Buat Pertanyaan
    </a>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(quest, key) in quests">
                <td> @{{ key+1 }} </td>
                <td> @{{ quest.judul }} </td>
                <td> @{{ quest.isi }} </td>
                <td>
                    <a v-bind:href="'/pertanyaan/'+ quest.id" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                    <a v-bind:href="'/pertanyaan/'+quest.id+'/edit'" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-sm btn-danger" v-on:click="removeTodo(key, quest)"><i class="fas fa-trash"></i></button>
                </td>
            </tr>  
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script>
  
</script>
<script>
    new Vue({
        el:"#pertanyaanku",
        data: {
            newQuest : "",
            quests : []
        },
        methods: {
            
            removeTodo : function(index, todo){
                this.$http.post('/api/quest/delete/' + todo.id).then(response => {
                    this.quests.splice(index, 1)
                });
            }

        },
        mounted: function() {
            // GET /someUrl
            this.$http.get('/api/quest').then(response => {

                // get body data
                let result = response.body;
                // console.log(result);
                this.quests = result;

            });
        }
    });
</script>
@endpush
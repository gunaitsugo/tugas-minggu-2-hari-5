@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Detail Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body" id="example2">
        <p>  Judul : @{{ quests.judul }} </p>
        <p>  Isi : @{{ quests.isi }} </p>
        <a v-bind:href="'/jawaban/create/'+ quests.id" class="btn btn-primary mb-2">
        Jawab
        </a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Jawaban</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(answer, key) in answers">
                    <td> @{{ key+1 }} </td>
                    <td> @{{ answer.isi }} </td>
                    <td>
                        <a v-bind:href="'/jawaban/'+ answer.id + '/edit'" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                        <button class="btn btn-sm btn-danger" v-on:click="removeTodo(key, answer)"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Jawaban</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection


@push('scripts')
<script>
    new Vue({
        el:"#example2",
        data: {
            newQuest : "",
            quests : [],
            answers : []
        },
        methods: {
            removeTodo : function(index, answer){
                this.$http.post('/api/answer/delete/' + answer.id).then(response => {
                    this.answers.splice(index, 1)
                });
            }

        },
        mounted: function() {
            // GET /someUrl
            this.$http.post('/api/quest/' + {{$id}}).then(response => {

                // get body data
                let resultQuest = response.body.quest;
                let resultAnswer = response.body.answers;
                // console.log(resultQuest);
                this.quests = resultQuest;
                this.answers = resultAnswer;

            });
        }
    });
</script>
@endpush
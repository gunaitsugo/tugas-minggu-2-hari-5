<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('quest', 'PertanyaanController@index');
Route::post('quest/{id}', 'PertanyaanController@show');
Route::post('quest', 'PertanyaanController@store');
Route::post('quest/update/{id}', 'PertanyaanController@update');
Route::post('quest/delete/{id}', 'PertanyaanController@destroy');


Route::post('answer', 'JawabanController@store');
Route::post('answer/{id}', 'JawabanController@edit');
Route::post('answer/update/{id}', 'JawabanController@update');
Route::post('answer/delete/{id}', 'JawabanController@destroy');
